//
//  Item.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * filter1;
@property (nonatomic, retain) NSString * itemdescription;
@property (nonatomic, retain) NSData * media;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * subcategory;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) User *user;

@end
