//
//  User.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * emailId;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSManagedObject *items;

@end
