//
//  UIViewController+ActionSheetSimulation.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 06/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ActionSheetSimulation)

-(UIView*) actionSheetSimulationWithPickerView:(UIPickerView*)pickerView withToolbar: (UIToolbar*)pickerToolbar;
-(void)dismissActionSheetSimulation:(UIView*)actionSheetSimulation;

@end
