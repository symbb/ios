//
//  CategoryEntity.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface CategoryEntity : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) Item *toitem;

@end
