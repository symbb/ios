//
//  Cities.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Cities : NSManagedObject

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * counutry;
@property (nonatomic, retain) NSString * region;
@property (nonatomic, retain) NSManagedObject *items;

@end
