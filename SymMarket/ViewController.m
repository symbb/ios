//
//  ViewController.m
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import "ViewController.h"
#import "Cities.h"
#import "CategoryEntity.h"
#import "SymcCell.h"
#import "AppDelegate.h"
#import "UIViewController+ActionSheetSimulation.h"
#import "SecondVCViewController.h"

@interface ViewController ()
@property (nonatomic,assign) IBOutlet UICollectionView* collectionView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSError *error;
    
    if (![[self fetchedCityController] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
    
    
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
    
    
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    AppDelegate *sharedDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([sharedDelegate fetchedResultsController] != nil) {
        return [sharedDelegate fetchedResultsController];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"CategoryEntity" inManagedObjectContext:[sharedDelegate managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"title" ascending:NO];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[sharedDelegate managedObjectContext] sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    
    sharedDelegate.fetchedResultsController = theFetchedResultsController;
    
    return sharedDelegate.fetchedResultsController;
    
}

- (NSFetchedResultsController *)fetchedCityController{
    AppDelegate *sharedDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([sharedDelegate fetchedCityController] != nil) {
        return [sharedDelegate fetchedCityController];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Cities" inManagedObjectContext:[sharedDelegate managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"city" ascending:NO];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[sharedDelegate managedObjectContext] sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    
    sharedDelegate.fetchedCityController = theFetchedResultsController;
    
    return sharedDelegate.fetchedCityController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    AppDelegate *sharedDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"CategoryEntity" inManagedObjectContext:moc]];
    [request setPredicate:nil];
    
    [request setIncludesSubentities:NO];

    NSError *err;
    categoryArray = [moc executeFetchRequest:request error:&err];
    

    NSLog(@"%lu",(unsigned long)[categoryArray count]);
    return [categoryArray count];
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)configureCell:(SymcCell *)cell atIndexPath:(NSIndexPath *)indexPath {
     AppDelegate *sharedDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CategoryEntity *info = [[sharedDelegate fetchedResultsController] objectAtIndexPath:indexPath];
    cell.category.text = info.title;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    SymcCell *cell = (SymcCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SymcCell" forIndexPath:indexPath];
    // Set up the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SecondVCViewController* secondVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondVCViewController"];
    [self.navigationController pushViewController:secondVC animated:YES];
}
- (IBAction)cityclicked:(id)sender {
    [self createPickerView];
}

- (void) createPickerView {
    maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [maskView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
    
    [self.view addSubview:maskView];
    _providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 344, self.view.bounds.size.width, 44)];
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet:)];
    _providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
    _providerToolbar.barStyle = UIBarStyleBlackOpaque;
    [self.view addSubview:_providerToolbar];
    
    _providerPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 300, 0, 0)];
    _providerPickerView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    _providerPickerView.showsSelectionIndicator = YES;
    _providerPickerView.dataSource = self;
    _providerPickerView.delegate = self;
    
    [self.view addSubview:_providerPickerView];
}

- (void)dismissActionSheet:(id)sender{
    [maskView removeFromSuperview];
    [_providerPickerView removeFromSuperview];
    [_providerToolbar removeFromSuperview];
}



#pragma pickerview delegates
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    AppDelegate *sharedDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Cities" inManagedObjectContext:moc]];
    [request setPredicate:nil];
    
    [request setIncludesSubentities:NO];
    
    NSError *err;
    categoryArray = [moc executeFetchRequest:request error:&err];
    
    
    NSLog(@"city count == %lu",(unsigned long)[categoryArray count]);
    return [categoryArray count];

}


- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    AppDelegate *sharedDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"%lu",(unsigned long)row);
    Cities *info = [[sharedDelegate fetchedResultsController] objectAtIndexPath:[NSIndexPath indexPathWithIndex:row]];

    NSLog(@"This is it: %@", info.city);

    return info.city;
}
@end
