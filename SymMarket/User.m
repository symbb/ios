//
//  User.m
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic emailId;
@dynamic username;
@dynamic items;

@end
