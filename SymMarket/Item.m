//
//  Item.m
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import "Item.h"
#import "User.h"


@implementation Item

@dynamic category;
@dynamic filter1;
@dynamic itemdescription;
@dynamic media;
@dynamic price;
@dynamic subcategory;
@dynamic title;
@dynamic user;

@end
