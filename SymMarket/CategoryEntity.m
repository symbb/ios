//
//  CategoryEntity.m
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import "CategoryEntity.h"
#import "Item.h"


@implementation CategoryEntity

@dynamic title;
@dynamic desc;
@dynamic toitem;

@end
