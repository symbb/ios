//
//  ViewController.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIPickerViewDataSource,UIPickerViewDelegate>{
    NSArray *categoryArray;
    NSArray *cityArray;

    IBOutlet UIButton *selectCity;
    UIView *maskView;
    UIPickerView *_providerPickerView;
    UIToolbar *_providerToolbar;
}
- (IBAction)cityclicked:(id)sender;

-(UIView*) actionSheetSimulationWithPickerView:(UIPickerView*)pickerView withToolbar: (UIToolbar*)pickerToolbar;
-(void)dismissActionSheetSimulation:(UIView*)actionSheetSimulation;


@end

