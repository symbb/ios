//
//  cell.h
//  SymMarket
//
//  Created by Visweswaran Chidambaram on 05/11/14.
//  Copyright (c) 2014 Visweswaran Chidambaram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;

@end
